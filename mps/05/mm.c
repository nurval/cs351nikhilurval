/*
 * mm-naive.c - The fastest, least memory-efficient malloc package.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#include "mm.h"
#include "memlib.h"

/* single word (4) or double word (8) alignment */
#define ALIGNMENT 8

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~0x7)

#define MIN_BLK_SIZE (ALIGN(sizeof(bH)))

#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

 //implicit function declarations
void *find_fit(size_t);
void *coalesce(size_t);

//applies to free blocks. occupied blocks only keep size info
typedef struct blockHeader{
  size_t size;
  struct blockHeader *next;
  struct blockHeader *prev;
} bH;

/* 
 * mm_init - initialize the malloc package.
 */
int mm_init(void)
{
  bH *bp = mem_sbrk(MIN_BLK_SIZE);
  bp -> size = MIN_BLK_SIZE;
  bp -> next = bp;
  bp -> prev = bp;
  return 0;
}

/* 
 * mm_malloc - Allocate a block by incrementing the brk pointer.
 *     Always allocate a block whose size is a multiple of the alignment.
 */
void *mm_malloc(size_t size)
{
  int newsize = ALIGN(size + MIN_BLK_SIZE);
  bH *bp = find_fit(newsize);
  if (bp == NULL){
    bp = mem_sbrk(newsize);
    if (bp == (void *)-1){
      return NULL;
    }else{
      bp -> size = newsize;
      bp -> size |= 1;
    }
  }else {
    bp -> size |= 1;
    bp -> prev -> next = bp -> next;
    bp -> next -> prev = bp -> prev;
  }
  return (char *) bp + MIN_BLK_SIZE;
}

void *find_fit(size_t size)
{
  bH *bp = ((bH *)mem_heap_lo())->next;
  while (bp != mem_heap_lo() && bp -> size < size) {
    bp = bp -> next;
  }
  if (bp == mem_heap_lo()){
    return NULL;
  }else{
    return bp;
  }
}

/*
 * mm_free - Freeing a block does nothing.
 */
void mm_free(void *ptr)
{
  bH *bp = ptr - MIN_BLK_SIZE;
  bH *head = mem_heap_lo();
  bp -> size = bp -> size & ~1;
  bp -> prev = head;
  bp -> next = head -> next;
  bp -> next -> prev = bp;
  head -> next = bp;
  //coalesce(bp);
}

/*
 * mm_realloc - Implemented simply in terms of mm_malloc and mm_free
 */
void *mm_realloc(void *ptr, size_t size)
{
  // If null pointer just run malloc. That function handles it.
  if (!ptr) {
    return mm_malloc(size);
  }

  bH *bp = (bH *)ptr-1;
  // If for some reason, the realloc size is smaller than the current size:
  if (bp -> size >= size){
    return ptr;
  }

  // This is where my realloc sucks. It just calls malloc for the new size.
  void *temP = mm_malloc(size);
  if (!temP){
    return NULL;
  }
  // Copying the data to the new space. 
  memcpy(temP, ptr, bp->size);
  // clear the ptr space, and return the temp pointer.
  mm_free(ptr);
  return temP;
}


//4 possible outcomes if block y is freed.
//1. x is also free, but z is not: coalesce x and y
//2. x is not free, but z is: coalesce y and z
//3. x is free, and z is free: coalesce x, y, and z
//4. y is at the end of the heap: only coalesce backwards if possible.
// void *coalesce(bH *bp) {
//   //create the next and previous blockHeader pointers using the built in struct
//   //for the previous, I will subtract size_t_size from the address to bring
//   //me to the size footer of the previous block. 
//   void *prev = 
//   //for the next, I will add both the size of the current block to bring me
//   //to the size header of the next block.
  
//   //Determine whether they are allocated
//   //In both cases, I just have to check the allocated bit to see if they are free. Store the pointers in variables.


//   //next, I will use if/else logic to guide the coalesce function based on the possible outcomes.


// }
#include <stdio.h>
#include <stdlib.h>

//Structs
typedef struct {
    //includes tag, data, valid, index
    unsigned int tag;
    int valid;
    int mostRecent;
} cache_l;

typedef struct {
    cache_l *lines; //number of lines
} cache_s;

typedef struct {
    int num_sets;   //number of sets
    int block_size; //bytes per block
    int assoc; //lines per set
    cache_s *sets; //number of sets
} cache_t;

//declarations
static cache_t *alloc_cache(int lines, int lps, int bs);
static int search_cache(long addr, cache_t *c);

int main (int argc, char *argv[]) {
    int ahit = 0;
    int num_hits = 0;
    double hit_rate;
    double miss_rate;
    int num_lines = atoi(argv[1]),
        lines_per_set = atoi(argv[2]),
        bytes_per_block = atoi(argv[3]);
    cache_t *c = alloc_cache(num_lines, lines_per_set, bytes_per_block);
    char line[80]; //buffer of 80 chars
    long addr_req;
    int x = 0;
    while (fgets(line, 80, stdin)) {
        addr_req = strtol(line, NULL, 0);
        ahit = search_cache(addr_req, c);
        if (ahit){
            num_hits++;
        }
        x++;
        hit_rate = 100 * (double)num_hits/x;
        miss_rate = 100 * (x - (double)num_hits)/x;
    }
    printf("Hit rate: %0.2f | Miss rate: %0.2f\n", hit_rate, miss_rate);
    return 0;
}

static int search_cache(long addr, cache_t *c){
    static unsigned int last = 0;
    int shift = (addr/(c->block_size)) % c->num_sets;
    int cacheTag = (addr/(c->block_size)) / c->num_sets;
    int hit = 0;
    cache_s *s = c->sets+shift;
    int i;
    for (i=0; i < c->assoc; i++){
        if (s->lines[i].valid && s->lines[i].tag == cacheTag){
            hit = 1;
            break;
        }
    }
    if (hit){
        s->lines[i].mostRecent = last++;
        return 1;
    }
    else {
        int empty = 0;
        int leastRecent = s->lines[0].mostRecent;
        int j;
        for (j=0; j < c->assoc; j++){
            if (s->lines[i].mostRecent < leastRecent){
                leastRecent = s->lines[i].mostRecent;
                empty = i;
            }
            if (!(s->lines[i].valid)){
                s->lines[i].valid = 1;
                s->lines[i].mostRecent = last++;
                s->lines[i].tag = cacheTag;
                return 0;
            }
        }
        s->lines[empty].valid = 1;
        s->lines[empty].tag = cacheTag;
        s->lines[empty].mostRecent = last++;
        return 0;
    }
}

static cache_t *alloc_cache(int lines, int lps, int bs){
    cache_t *c = malloc(sizeof(cache_t));
    cache_l *cline;
    cache_s *cset;
    
    c->num_sets = lines/lps;
    c->assoc = lps;
    c->block_size = bs;
    c->sets = malloc(lines*sizeof(cache_s));
    
    int i, j;
    for (i=0; i < c->num_sets; i++){
        cset = &c->sets[i];
        (c->sets+i)->lines = malloc(lps*sizeof(cache_l));
        for (j=0; j < lps; j++){
            cline = &((c->sets+i)->lines[j]);
            cline->valid = 0;
        }
    }
    return c;
}


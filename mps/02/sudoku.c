#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sudoku.h"

#define NUM_DIGITS 9
#define NUM_ROWS   NUM_DIGITS
#define NUM_COLS   NUM_DIGITS
#define NUM_PEERS  20
#define NUM_UNITS  3
#define DIGITS     "123456789"
#define ROW_NAMES  "ABCDEFGHI"
#define COL_NAMES  DIGITS

typedef struct square {
  char vals[NUM_DIGITS+1]; // string of possible values
  unsigned char row;
  unsigned char col;
  struct square *peers[NUM_PEERS];
  struct square *units[NUM_UNITS][NUM_DIGITS];
} square_t;

typedef struct puzzle {
  square_t squares[NUM_ROWS][NUM_COLS];
} puzzle_t;

void solve(unsigned char grid[9][9]);

// following are static ("private") function declarations --- add as needed

static puzzle_t *create_puzzle(unsigned char grid[9][9]);
static void init_peers(puzzle_t *puz, int row, int col);
static puzzle_t *copy_puzzle(puzzle_t *puz);
static void free_puzzle(puzzle_t *puz);
static void print_puzzle(puzzle_t *);

static puzzle_t *search(puzzle_t *puz);
static puzzle_t *assign(puzzle_t *puz, int row, int col, char val);
static puzzle_t *eliminate(puzzle_t *puz, int row, int col, char val);

/*************************/
/* Public solve function */
/*************************/

void solve(unsigned char grid[9][9]) {
  puzzle_t *puz = create_puzzle(grid);
  puzzle_t *solved;
  if ((solved = search(puz)) != NULL) {
    print_puzzle(solved);
  }
  free_puzzle(puz);
}

/*******************************************/
/* Puzzle data structure related functions */
/*******************************************/

static puzzle_t *create_puzzle(unsigned char vals[9][9]) {
  puzzle_t *newp = malloc(sizeof(puzzle_t));
  int i, j;
  for (i=0; i<NUM_ROWS; i++) {
    for (j=0; j<NUM_COLS; j++) {
      // if(vals[i][j] == '0'){
      //   for(int x=0; x<NUM_DIGITS; x++){
      //     newp->squares[i][j].vals[x] = (char)x;
      //   }
      // }else{
        newp = assign(newp, i, j, vals[i][j]);
      // }
      init_peers(newp, i, j);
    }
  }
  print_puzzle(newp);
  return newp;
}

static void init_peers(puzzle_t *puz, int row, int col) {
  //add squares from current unit to peers/units
  // int unitCount = 0;
  // int peerCount = 0;
  // int unitStartRow = row - (row%3);
  // int unitEndRow = unitStartRow+2;
  // int unitStartCol = col - (col%3);
  // int unitEndCol = unitStartCol+2;
  // for(int i = unitStartRow; i<=unitEndRow; i++){
  //   for(int j = unitStartCol; i<=unitEndCol; j++){
  //     //puz->squares[row][col].units[unitCount] = &(puz->squares[i][j]);
  //     if(!(i==row && j==col)){
  //       puz->squares[row][col].peers[peerCount] = &(puz->squares[i][j]);
  //       peerCount++;
  //     }
  //   }
  // }
}

static void free_puzzle(puzzle_t *puz) {
  free(puz);
}

static puzzle_t *copy_puzzle(puzzle_t *puz) {
  puzzle_t *copy = puz;
  return copy;
}

void print_puzzle(puzzle_t *p) {
  int i, j;
  for (i=0; i<NUM_ROWS; i++) {
    for (j=0; j<NUM_COLS; j++) {
      printf(" %9s", p->squares[i][j].vals); // may be useful while debugging
      //printf(" %2s", p->squares[i][j].vals);
    }
    printf("\n");
  }
}

/**********/
/* Search */
/**********/

static puzzle_t *search(puzzle_t *puz) {
  return NULL;
}

/**************************/
/* Constraint propagation */
/**************************/

static puzzle_t *assign(puzzle_t *puz, int row, int col, char val) {
  //"""Eliminate all the other values (except d) from values[s] and propagate.
  //  Return values, except return False if a contradiction is detected."""
  puz->squares[row][col].vals[0] = val;
  for(int i = 0; i<NUM_DIGITS; i++){
    if (strchr(puz->squares[row][col].vals, (char)i) && i!=(int)val){
      puz = eliminate(puz, row, col, i);
    }
  }
  return puz;
}

static puzzle_t *eliminate(puzzle_t *puz, int row, int col, char val) {
  //"""Eliminate d from values[s]; propagate when values or places <= 2.
  //  Return values, except return False if a contradiction is detected."""
  if (strchr((puz->squares[row][col]).vals, val)){
    
  }


  return puz;
}

/*****************************************/
/* Misc (e.g., utility) functions follow */
/*****************************************/
